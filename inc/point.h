#ifndef QTREE_POINT_H
#define QTREE_POINT_H 

namespace qtree {
	
using num_t = double;

class Point {
private: 
	num_t m_x = 0;
	num_t m_y = 0;

public:
	Point() = default;

	Point(const num_t& x, const num_t& y) : m_x(x), m_y(y) {}
	Point(const num_t& x, num_t&& y)			: m_x(x), m_y(y) {}
	Point(num_t&& x, const num_t& y)			: m_x(x), m_y(y) {}
	Point(num_t&& x, num_t&& y)           : m_x(x), m_y(y) {}

	Point(const Point& point)             : m_x(point.m_x), m_y(point.m_y) {}
	Point(Point&& point)                  : m_x(point.m_x), m_y(point.m_y) {}

	void setX(const num_t& x) { m_x = x; }
	void setY(const num_t& y) { m_y = y; }

	num_t getX() const { return m_x; }
	num_t getY() const { return m_y; }
};

}

#endif
