cmake_minimum_required(VERSION 3.7)
project(QuadTree)

set(CMAKE_CXX_STANDARD 14)

file(GLOB SOURCES "src/*.cc")
file(GLOB HEADERS "inc/*/*.h")
file(GLOB TESTSRC "test/*.cc")

include_directories(inc)

add_executable(qtree ${TESTSRC} ${HEADERS})
